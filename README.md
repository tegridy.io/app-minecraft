# Minecraft Spigot Image

This image starts a minecraft spigot server.  
"Optimized" to run on Kubernetes.

## Build

To build the image with the latest stable release:
```shell
./build.sh
```

To build the image with custom release:
```shell
./build.sh 1.17.1
```

## Configuration

To load a set of plugins create a file `/minecraft/plugins.list` with the following content:
```
dynmap,http://mikeprimm.com/dynmap/builds/dynmap/Dynmap-HEAD-spigot.jar
dynmap-mobs,http://mikeprimm.com/dynmap/builds/dynmap-mobs/dynmap-mobs-HEAD.jar
```

To test the image in docker:
```shell
docker run --name minecraft -v $PWD/test:/minecraft -d registry.gitlab.com/tegridy.io/app-minecraft:1.17.1
```

## Kubernetes Example

The basic manifest can be kustomized to the clusterenvironment.

namespace.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: minecraft
```

patches/pvc.yaml
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  annotations:
    volume.beta.kubernetes.io/storage-class: nfs-client
    volume.beta.kubernetes.io/storage-provisioner: cluster.local/nfs-subdir-external-provisioner
  name: minecraft-data
spec:
  storageClassName: nfs-client
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 50Gi
```

patches/deployment.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: minecraft
spec:
  template:
    spec:
      restartPolicy: Always
      containers:
      - name: minecraft
        imagePullPolicy: Always
```

kustomization.yaml
```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

bases:
- https://gitlab.com/tegridy.io/app-minecraft/manifests/

resources:
- namespace.yaml

patches:
- patches/pvc.yaml
- patches/deployment.yaml

namespace: minecraft

images:
- name: registry.gitlab.com/tegridy.io/app-minecraft
  newTag: 1.17.1
```

## Advanced Kubernetes Example

Additional to the above example, you can kustomize the deployment further

### Whitelist

config-minecraft.yaml
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: minecraft-config
data:
  whitelist.json: |
    [
      {
        "uuid": <uuid>,
        "name": <name>
      }
    ]   
  server.properties: |
    spawn-protection=0
    max-tick-time=60000
    query.port=25565
    generator-settings=
    force-gamemode=false
    allow-nether=true
    enforce-whitelist=true
    gamemode=survival
    broadcast-console-to-ops=true
    enable-query=false
    player-idle-timeout=0
    difficulty=easy
    spawn-monsters=true
    broadcast-rcon-to-ops=true
    op-permission-level=4
    pvp=true
    snooper-enabled=true
    level-type=default
    hardcore=false
    enable-command-block=true
    max-players=20
    network-compression-threshold=256
    resource-pack-sha1=
    max-world-size=29999984
    function-permission-level=2
    rcon.port=25575
    server-port=25565
    debug=false
    server-ip=
    spawn-npcs=true
    allow-flight=false
    level-name=world
    view-distance=16
    resource-pack=
    spawn-animals=true
    white-list=true
    rcon.password=
    generate-structures=true
    online-mode=true
    max-build-height=256
    level-seed=
    prevent-proxy-connections=false
    use-native-transport=true
    motd=A Minecraft Server
    enable-rcon=false
```

patches/config-minecraft.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: minecraft
spec:
  template:
    spec:
      restartPolicy: Always
      containers:
      - name: minecraft
          volumeMounts:
          - name: config
            mountPath: /minecraft
      volumes:
      - name: config
        configMap:
          name: minecraft-config
```

kustomization.yaml
```yaml
...
resources:
- config-minecraft.yaml

patches:
- patches/config-minecraft.yaml
...
```

### Plugins


config-minecraft.yaml
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: minecraft-config
data:
  plugins.list: |
    dynmap,http://mikeprimm.com/dynmap/builds/dynmap/Dynmap-HEAD-spigot.jar
    dynmap-mobs,http://mikeprimm.com/dynmap/builds/dynmap-mobs/dynmap-mobs-HEAD.jar
```

patches/config-minecraft.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: minecraft
spec:
  template:
    spec:
      restartPolicy: Always
      containers:
      - name: minecraft
        volumeMounts:
        - name: config
          mountPath: /minecraft
      volumes:
      - name: config
        configMap:
          name: minecraft-config
```
