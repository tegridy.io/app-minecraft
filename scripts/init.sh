#!/bin/bash

JVM_OPTS="$JVM_OPTS -Dfile.encoding=UTF8"
PLUGIN_LIST=/minecraft/plugins.list

# download plugins
if test -f $PLUGIN_LIST; then
    echo "$PLUGIN_LIST found, downloading plugins."
    mkdir -p /minecraft/plugins
    while IFS=, read -r PLUGIN_NAME PLUGIN_URL; do
      PLUGIN_TARGET=/minecraft/plugins/$PLUGIN_NAME.jar
      if test -f $PLUGIN_TARGET; then
        echo "downloading $PLUGIN_URL -> $PLUGIN_TARGET"
        curl -s $PLUGIN_URL -o $PLUGIN_TARGET
      fi
    done <$PLUGIN_LIST
fi

# prepare minecraft
# cp -r /minecraft_orig/* /minecraft/
echo "eula=true" > /minecraft/eula.txt

# start minecraft
cd /minecraft
/run.sh java $JVM_OPTS -jar /minecraft_orig/spigot.jar nogui
